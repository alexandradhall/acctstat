import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Home from './pages/Home';
import Income from './pages/Income';
import Balance from './pages/Balance';
import Earnings from './pages/Earnings';
import Flows from './pages/Flows';
import Examples from './pages/Examples';

const Main = () => {
  return (
    <Switch> {/* The Switch decides which component to show based on the current URL.*/}
      <Route exact path='/' component={Home}></Route>
      <Route exact path='/examples' component={Examples}></Route>
      <Route exact path='/income' component={Income}></Route>
      <Route exact path='/balance' component={Balance}></Route>
      <Route exact path='/earnings' component={Earnings}></Route>
      <Route exact path='/flows' component={Flows}></Route>
    </Switch>
  );
}

export default Main;