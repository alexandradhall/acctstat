import React from 'react';
import IncomeStat from './img/ExIncomeStat.jpg';
import RetEarnings from './img/ExRetEarnings.jpg';
import BalanceStat from './img/ExBalance.jpg';
import CashFlow from './img/ExCashFlows.jpg';
const Examples = () => {
    return (
        <div>
            <img src={IncomeStat} alt="Income Statement" height="518" width="900" /> <br></br>
            <img src={RetEarnings} alt="Retained Earnings" height="353" width="901" /><br></br>
            <img src={BalanceStat} alt="Balance Statement" height="434" width="1074" /><br></br>
            <img src={CashFlow} alt="Cash Flows" height="394" width="550" /><br></br>
        </div>
    );
};
export default Examples;