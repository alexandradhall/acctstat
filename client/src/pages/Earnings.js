import "../Report.css";
import { useState, useEffect, useRef } from "react";
import axios from "axios";

const Earnings = () => {
    const [owner, setOwner] = useState("");
    const [statementType, setStatement] = useState("Retained Earnings");
    const [fiscalPeriod, setPeriod] = useState("");
    const [fiscalEnd, setEndPeriod] = useState("");
    const [fiscalYear, setYear] = useState(new Date().getFullYear());
    const [fiscalMonth, setMonth] = useState("");
    const [net, setNet] = useState(0);
    const [earnings, setEarnings] = useState(0);

    const [record, setRecord] = useState([]);
    const [setTotal] = useState([]);

    //input validation

    const firstRender = useRef(true);

    const [disable, setDisabled] = useState(true);

    //errors
    const [nameError, setNameError] = useState(null);
    const [periodError, setPeriodError] = useState(null);
    const [dateError, setDateError] = useState(null);
    const [yearError, setYearError] = useState(null);
    const [monthError, setMonthError] = useState(null);
    useEffect(() => {
        const formValidation = () => {
            let valid = true;
            if (owner === "") {
                setNameError("Name can't be blank!")
                valid = true;
            } else if (owner !== "") {
                setNameError(null);
                valid = false;
            }
            if (fiscalPeriod === "") {
                setPeriodError("Must input a fiscal period.")
                valid = true;
            } else if (fiscalPeriod !== "") {
                setPeriodError(null)
                valid = false;
            }
            if (fiscalEnd === "") {
                setDateError("Must input a fiscal end.")
                valid = true;
            } else if (fiscalEnd !== "") {
                setDateError(null)
                valid = false;
            }
            if (fiscalYear === "") {
                setYearError("Must input a fiscal year.")
                valid = true;
            } else if (fiscalYear !== "") {
                setYearError(null)
                valid = false;
            }

            if (fiscalMonth === "") {
                setMonthError("Must input a fiscal month.")
                valid = true;
            } else if (fiscalMonth !== "") {
                setMonthError(null)
                valid = false;
            }

            return valid;
        }

        if (firstRender.current) {
            firstRender.current = false
            return
        }

        setDisabled(formValidation());


    }, [owner, fiscalPeriod, fiscalEnd, fiscalMonth, fiscalYear, statementType])


    const getRecords = (owner, statementType, fiscalMonth, fiscalYear) => {
        var query;
        if (!fiscalYear && fiscalMonth) {
            let fiscalYear = new Date().getFullYear();
            query = `SELECT * FROM records WHERE owner='${owner}' AND statementType= '${statementType}' AND \
             fiscalMonth= '${fiscalMonth} AND fiscalYear = ${fiscalYear}'; SELECT category, SUM(monetaryValue) As\
             Total FROM test WHERE statementType= 'Income Statement' AND \
             fiscalYear = ${fiscalYear} AND fiscalMonth = '${fiscalMonth}' \
             GROUP BY category; SELECT category, SUM(monetaryValue) As\
             Total FROM test WHERE owner='${owner}' AND statementType= '${statementType}' AND \
             fiscalYear = ${fiscalYear} AND fiscalMonth = '${fiscalMonth}' \
             GROUP BY category;`;
        } else if ((!fiscalMonth && fiscalYear) || (!fiscalMonth && !fiscalYear)) {
            query = `SELECT * FROM records WHERE owner='${owner}' AND statementType= '${statementType}' AND \
            fiscalYear = ${fiscalYear} ; SELECT category, SUM(monetaryValue) As\
            Total FROM test WHERE statementType= 'Income Statement' AND fiscalYear = ${fiscalYear} \
            GROUP BY category; SELECT category, SUM(monetaryValue) As\
            Total FROM test WHERE owner='${owner}' AND statementType= '${statementType}' AND fiscalYear = ${fiscalYear} \
            GROUP BY category;`;
        } else if (fiscalYear && fiscalMonth) {
            query = `SELECT * FROM records WHERE AND statementType= '${statementType}' AND \
            fiscalYear = ${fiscalYear} AND fiscalMonth = '${fiscalMonth}' AND owner='${owner}'; SELECT category, SUM(monetaryValue) As\
            Total FROM test WHERE statementType= 'Income Statement' AND \
            fiscalYear = ${fiscalYear} AND fiscalMonth = '${fiscalMonth}' \
            GROUP BY category AND owner='${owner}';  SELECT category, SUM(monetaryValue) As\
            Total FROM test WHEREAND statementType= '${statementType}' AND \
            fiscalYear = ${fiscalYear} AND fiscalMonth = '${fiscalMonth}' AND owner='${owner}' \
            GROUP BY category;`;
        }
        console.log(query);
        axios.get("http://localhost:3001/generate/earnings", {
            params: {
                query: query,
                fiscalMonth: fiscalMonth,
                fiscalYear: fiscalYear
            }
        }).then((response) => {
            setRecord(response.data[0]);
            setTotal(response.data[1]);
            setNet(response.data[1][0].Total - response.data[1][1].Total);
            setEarnings((net + response.data[2][0].Total) - response.data[2][1].Total)
        }).catch((response) => {
            console.log(response);
            alert("An error has occurred. Please check your information.");
        })

    };

    return (
        <div className="App">
            <div> Instructions:
                <ol>
                    <li> Enter name of business or your name as you did on when adding the record.</li>
                    <li> Enter the fiscal year and/or month for the records you'd like to see. </li>
                    <li> Enter the desired fiscal period, either "Month" or "Year".</li>
                    <li> Enter end date you'd like on the statement</li>
                    <li> Click Show Records.</li>
                </ol>
            </div>
            <div className="form">

                <div className="form-fieldset">
                    <div className="field">
                        <label>Name:</label>
                        <input
                            type="text"
                            value={owner}
                            onChange={(event) => {
                                setOwner(event.target.value);
                            }}
                        />
                        {nameError && <error>{nameError}</error>}
                    </div>
                    <div className="field">
                        <label >Statement Type:</label>
                        <select value={statementType} onChange={(event) => {
                            setStatement(event.target.value);
                        }}>
                            <option value="Income Statement">Income Statement</option>
                            <option value="Balance Sheet">Balance Sheet</option>
                            <option value="Retained Earnings">Retained Earnings</option>
                            <option value="Cash Flows">Cash Flows</option>
                        </select>
                    </div>
                    <div className="field">
                        <label>Fiscal Year:</label>
                        <input
                            type="number"
                            value={fiscalYear}
                            onChange={(event) => {
                                setYear(event.target.value);
                            }}
                        />
                        {yearError && <error>{yearError}</error>}
                    </div>
                    <div className="field">
                        <label>Fiscal Month:</label>
                        <input
                            type="text"
                            value={fiscalMonth}
                            onChange={(event) => {
                                setMonth(event.target.value);
                            }}
                        />
                        {monthError && <error>{monthError}</error>}
                    </div>
                    <div className="field">
                        <label style={{ fontSize: "9px" }}> Desired Fiscal Period (Year/Month):</label>
                        <input
                            type="text"
                            value={fiscalPeriod}
                            onChange={(event) => {
                                setPeriod(event.target.value);
                            }}
                        />
                        {periodError && <error>{periodError}</error>}
                    </div>
                    <div className="field">
                        <label style={{ fontSize: "9px" }}> Desired Period End (Date):</label>
                        <input
                            type="text"
                            value={fiscalEnd}
                            onChange={(event) => {
                                setEndPeriod(event.target.value);
                            }}
                        />
                        {dateError && <error>{dateError}</error>}
                    </div>
                </div>
                <button disabled={disable} onClick={() => {
                    getRecords(statementType, fiscalMonth, fiscalYear);
                }}>Show Records</button>
            </div>

            <div className="table">
                <div className="table-header">
                    <div>{owner}</div>
                    <div>Statement of {statementType}</div>
                    <div>For {fiscalPeriod} Ending {fiscalEnd} </div>
                </div>
                <div className="table-body">
                    <div className="row">
                        <div className="row-header">Retained Earnings, {fiscalMonth ? fiscalMonth : 'January'} 1, {fiscalYear}</div>
                        {record.map((val, key) => {
                            return (
                                <div className="column">
                                    {(() => {
                                        if (val.category === 'Retained Earnings') {
                                            return (
                                                <div >
                                                    ${val.monetaryValue}
                                                </div>
                                            )
                                        } else if (val.category === undefined || val.category === null) {
                                            return (
                                                <div > $0
                                                </div>
                                            )
                                        }
                                    })()}
                                </div>

                            )
                        })
                        }
                    </div>

                    <div className="row">
                        <div className="row-header">Net Income </div>
                        <div className="column">
                            <div className="total-value">${net}</div>
                        </div>
                        <div className="column"></div>
                    </div>
                    <div className="row">
                        <div className="row-header">Less: Dividends</div>
                    </div>
                    {record.map((val, key) => {
                        return (
                            <div className="record">
                                {(() => {
                                    if (val.category === 'Dividends') {
                                        return (
                                            <div className="record">
                                                <div className="row-header">{val.description}</div>
                                                <div className="column">${val.monetaryValue}</div>
                                            </div>
                                        )
                                    } else if (val.category === undefined || val.category === null) {
                                        return (
                                            <div className="record">
                                                <div className="column">$0</div>
                                            </div>
                                        )
                                    }
                                })()}
                            </div>

                        )
                    })
                    }
                    <div className="row">
                        <div className="row-header">Retained Earnings, {fiscalMonth ? fiscalMonth : 'December'} 30, {fiscalYear}</div>
                        <div className="column">
                            <div className="total-value">${earnings}</div>
                        </div>
                        <div className="column"></div>
                    </div>

                </div>
            </div>

        </div>
    );
}

export default Earnings