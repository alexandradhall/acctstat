

import "../index.css";
import { useState, useEffect, useRef } from "react";
import axios from "axios";
import { Link } from "react-router-dom";

const Home = () => {

	const [owner, setOwner] = useState("");
	const [statementType, setStatement] = useState("Income Statement");
	const [category, setCategory] = useState("");
	const [description, setDescription] = useState("");
	const [monetaryValue, setValue] = useState(0);
	const [startDate, setStart] = useState("");
	const [endDate, setEnd] = useState("");
	const [fiscalYear, setYear] = useState("");
	const [fiscalMonth, setMonth] = useState("");

	const [record, setRecord] = useState([]);

	const firstRender = useRef(true);

	const [disable, setDisabled] = useState(true);

	//errors
	const [nameError, setNameError] = useState(null);
	const [categoryError, setCatError] = useState(null);
	const [descriptionError, setDescriptError] = useState(null);
	const [startError, setStartError] = useState(null);
	const [dateError, setDateError] = useState(null);
	const [yearError, setYearError] = useState(null);
	const [monthError, setMonthError] = useState(null);
	useEffect(() => {
		const formValidation = () => {
			let valid = true;
			if (owner === "") {
				setNameError("Name can't be blank!")
				valid = true;
			} else if (owner !== "") {
				setNameError(null);
				valid = false;
			}
			if (category === "") {
				setCatError("Must input a category.")
				valid = true;
			} else if (category !== "") {
				setCatError(null)
				valid = false;
			}
			if (description === "") {
				setDescriptError("Must input a description.")
				valid = true;
			} else if (description !== "") {
				setDescriptError(null)
				valid = false;
			}
			if (fiscalYear === "") {
				setYearError("Must input a fiscal year.")
				valid = true;
			} else if (fiscalYear !== "") {
				setYearError(null)
				valid = false;
			}

			if (fiscalMonth === "") {
				setMonthError("Must input a fiscal month.")
				valid = true;
			} else if (fiscalMonth !== "") {
				setMonthError(null)
				valid = false;
			}

			return valid;
		}

		if (firstRender.current) {
			firstRender.current = false
			return
		}

		setDisabled(formValidation());


	}, [owner, statementType, category, description, monetaryValue, startDate,
		endDate, fiscalYear, fiscalMonth])

	const addRecord = () => {
		console.log(owner + statementType + category + description + monetaryValue + startDate +
			endDate + fiscalYear + fiscalMonth);
		axios.post("http://localhost:3001/create", {
			owner: owner,
			statementType: statementType,
			category: category,
			description: description,
			monetaryValue: monetaryValue,
			startDate: startDate,
			endDate: endDate,
			fiscalYear: fiscalYear,
			fiscalMonth: fiscalMonth,
		}).then(() => {
			setRecord([
				...record,
				{
					owner: owner,
					statementType: statementType,
					category: category,
					description: description,
					monetaryValue: monetaryValue,
					startDate: startDate,
					endDate: endDate,
					fiscalYear: fiscalYear,
					fiscalMonth: fiscalMonth,
				},
			]);
		}).catch((response) => {
			console.log(response);
			alert("An error has occurred. Please check your information.");
		});
	};

	const getRecords = () => {
		axios.get("http://localhost:3001/records").then((response) => {
			setRecord(response.data);
		});
	};

	const updateRecord = (id) => {
		axios.put("http://localhost:3001/update", {
			owner: owner,
			statementType: statementType,
			category: category,
			description: description,
			monetaryValue: monetaryValue,
			startDate: startDate,
			endDate: endDate,
			fiscalYear: fiscalYear,
			fiscalMonth: fiscalMonth,
			id: id
		}).then(
			() => {
				setRecord(
					record.map((val) => {
						return val.id === id
							? {
								id: id,
								owner: owner,
								statementType: statementType,
								category: category,
								description: description,
								monetaryValue: monetaryValue,
								startDate: startDate,
								endDate: endDate,
								fiscalYear: fiscalYear,
								fiscalMonth: fiscalMonth,
							}
							: val;
					})
				);
			}
		);
	};

	const deleteRecord = (id) => {
		axios.delete(`http://localhost:3001/delete/${id}`).then((response) => {
			setRecord(
				record.filter((val) => {
					return val.id !== id;
				})
			);
		});
	};

	return (
		<div className="App">
			<div> Instructions:
				<ol>
					<li> Input your name or the name of your business below.</li>
					<li> Select the statement type you'd like to enter records for. </li>
					<li> Input your entry for each column. To see how best to enter values, click Examples above. </li>
					<li> Click Add to add to the list to records that will be used in the report.</li>
					<li> Show Records allows you to see all the records you've entered so far.</li>
					<li> To update a record, type in the record as you'd like it to look then click Update next to the record(after clicking Show Records).</li>
				</ol>
			</div>
			<div className="form">
				<div className="field">
					<label>Name:</label>
					<input
						type="text"
						value={owner}
						onChange={(event) => {
							setOwner(event.target.value);
						}}
						required
					/>
					{nameError && <error>{nameError}</error>}
				</div>
				<div className="field">
					<label >Statement Type:</label>
					<select value={statementType} required onChange={(event) => {
						setStatement(event.target.value);
					}}>
						<option value="Income Statement">Income Statement</option>
						<option value="Balance Sheet">Balance Sheet</option>
						<option value="Retained Earnings">Retained Earnings</option>
						<option value="Cash Flows">Cash Flows</option>
					</select>
				</div>

				<div className="form-fieldset">
					<div className="field">
						<label>Category:</label>
						<input
							type="text"
							value={category}
							onChange={(event) => {
								setCategory(event.target.value);
							}}
							required
						/>
						{categoryError && <error>{categoryError}</error>}
					</div>
					<div className="field">
						<label> Description:</label>
						<input
							type="text"
							value={description}
							onChange={(event) => {
								setDescription(event.target.value);
							}}
							required
						/>
						{descriptionError && <error>{descriptionError}</error>}
					</div>
					<div className="field">
						<label> Value:</label>
						<input
							type="number"
							value={monetaryValue}
							onChange={(event) => {
								setValue(event.target.value);
							}}
							required
						/>
					</div>
					<div className="field">
						<label> Start Date:</label>
						<input
							type="date"
							value={startDate}
							onChange={(event) => {
								setStart(event.target.value);
							}}
						/>
						{startError && <error>{startError}</error>}
					</div>
					<div className="field">
						<label>End Date:</label>
						<input
							type="date"
							value={endDate}
							onChange={(event) => {
								setEnd(event.target.value);
							}}
							required
						/>
						{dateError && <error>{dateError}</error>}
					</div>
					<div className="field">
						<label>Fiscal Year:</label>
						<input
							type="number"
							value={fiscalYear}
							onChange={(event) => {
								setYear(event.target.value);
							}}
							required
						/>
						{yearError && <error>{yearError}</error>}
					</div>
					<div className="field">
						<label>Fiscal Month:</label>
						<input
							type="text"
							value={fiscalMonth}
							onChange={(event) => {
								setMonth(event.target.value);
							}}
							required
						/>
						{monthError && <error>{monthError}</error>}
					</div>
				</div>
				<button onClick={addRecord} disabled={disable}>Add Record</button>
				<button onClick={getRecords}>Show Records</button>
			</div>

			<div className="table">
				<div className="table-header">
					<div className="row">
						<div className="column">Statement Type</div>
						<div className="column">Category</div>
						<div className="column">Description</div>
						<div className="column">Value</div>
						<div className="column">Start Date</div>
						<div className="column">End Date</div>
						<div className="column">Fiscal Year</div>
						<div className="column">Fiscal Month</div>
						<div className="column">Options</div>
					</div>
				</div>
				<div className="table-body">
					{record.map((val, key) => {
						return (
							<div className="row">
								<div className="column">{val.statementType}</div>
								<div className="column">{val.category}</div>
								<div className="column">{val.description}</div>
								<div className="column">{val.monetaryValue}</div>
								<div className="column">{val.startDate}</div>
								<div className="column">{val.endDate}</div>
								<div className="column">{val.fiscalYear}</div>
								<div className="column">{val.fiscalMonth}</div>
								<div className="column"><button
									onClick={() => {
										updateRecord(val.id);
									}}
								>
									Update
								</button>
									<button
										onClick={() => {
											deleteRecord(val.id);
										}}
									>
										Delete
									</button>
								</div>
							</div>
						)
					})
					}
				</div>
			</div>
			<div style={{
				display: "flex",
				justifyContent: "center",
				alignItems: "center"
			}} >
				<div style={{ display: "flex", justifyContent: "center", alignItems: "center" }} >
					<Link to="/income"><button>Income Statement</button></Link>
					<Link to="/balance"><button >Balance Sheet</button></Link>
					<Link to="/earnings"><button >Retained Earnings</button></Link>
					<Link to="/flows"><button >Cash Flow</button></Link>
				</div>
			</div>


		</div>
	);
}

export default Home;