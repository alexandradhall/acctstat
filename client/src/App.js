
import React from 'react';

import Main from './Main';
import Navigation from './Navigation';
import Header from './Header';

function App() {
  return (
    <div className="App">
      <Header />
      <Navigation />
      <Main />
    </div>
  );
};

export default App;