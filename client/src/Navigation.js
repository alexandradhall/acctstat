import React from 'react';
import { Link } from 'react-router-dom';

const Navigation = () => {
    return (
        <nav>
            <ul className="menu">
                <li className="link">
                    <Link to="/">Home</Link>
                </li>
                <li className="link">
                    <Link to="/examples">Examples</Link>
                </li>
            </ul>
        </nav>
    );
};
export default Navigation;